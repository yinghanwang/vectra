### What is this repository for? ###

* vectra test
* movie list

### Bonus feature ###

* local storage saving last search
* animation

### How do I get set up? ###

* install gulp, http-server, webpack globally, then
* npm install
* to build the app locally, run
* 1. webpack --watch
* 2. gulp

### Who do I talk to? ###

* Yinghan Wang
* wangx6@gmail.com