(function() {
	'use strict';

	let angular = require('angular');
	let movieStore = require('./models/movie-store.js');
	let movieRecord = require('./models/movie-record.js');

	// application
	let vec = angular.module('fll-movie', []);

	// models
	vec.factory('MovieRecord', movieRecord);
	vec.factory('MovieStore', movieStore);

	// controller
	vec.controller('movieListController', ['$scope', 'MovieStore', '$timeout', 
		function(s, MovieStore, timeout) {

		/**
		 * 
		 * @param
		 * @return
		 */
		s.store = new MovieStore();

		/**
		 * 
		 * @param
		 * @return
		 */
		s.inputValue = '';

		/**
		 * 
		 * @param
		 * @return
		 */
		s.isLoading = false;


		/**
		 * 
		 * @param
		 * @return
		 */
		s.onClickSearch = function(e) {
			s.isLoading = true;
			s.store.fetchMovies(s.inputValue).then(function() {
				s.isLoading = false;
			});
		};
		

	}]);

	// view component
	vec.directive('fllMovieItem', [function() {
		let link = function(s) {};

		return {
			restricted: 'E',
			link: link,
			scope: {
				list: '='
			},
			template: [
				'<div class="d-flex fll-movie-list flex-wrap">',
					'<div ng-repeat="i in list" class="fll-movie-list-container">',
						'<div class="fll-movie-list-wrap">',
							'<div class="fll-movie-list-item">',
								'<div class="fll-movie-list-item__poster" ng-class="i.hasImage ? \'\' : \'fll-no-image\'"><img ng-if="i.hasImage" src="{{i.data.Poster}}"/></div>',
								'<div class="fll-movie-list-item__text">',
									'<div class="fll-movie-list-item__title">{{i.data.Title}}</div>',
									'<div class="d-flex">',
										'<a href="https://www.imdb.com/title/{{i.data.imdbID}}/">',
											'<div>{{i.data.Type}}, {{i.data.Year}}</div>',
										'</a>',
									'</div>',
								'</div>',
							'</div>',
						'</div>',

					'</div>',
			
				'</div>',
			].join('')
		};
	}]);

})();