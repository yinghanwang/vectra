/**
 * 
 * @param
 * @return
 */
module.exports = [function() {
	function MovieRecord(config) {
		config = config || {};
		this.data = config.data;
		this.hasImage = false;
		this.init();
	}

	let p = MovieRecord.prototype;

	p.init = function() {
		if(this.data) {
			this.hasImage = !(this.data.Poster === 'N/A' || !this.data.Poster)
		}
	}
	return MovieRecord;
}];


