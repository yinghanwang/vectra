(function() {
	'use strict';

	var util = require('../services/util');


	var BaseModel = (function(_super) {

		util.extends(BaseModel, _super);

		function BaseModel(config) {
			_super.call(this, config);
			this.config = config || {};
			this.sorters = null;
			this.filters = null;
			this.bindedView = [];
			this.records = [];
			this.filtered = null;
			this.raw = null;
		}	

		var p = BaseModel.prototype;

		/**
		 * 
		 * @param {}
		 */
		p.init = function() {
			return this;
		};


		/**
		 * 
		 * @param {}
		 */
		p.update = function() {
			for(var i = 0 ;i < this.bindedView.length; i++) {
				this.bindedView[i].setModel(this);
			}
			console.log(this);
		};

		p.isEmpty = function() {
			return !this.records || !this.records.length;
		};

		/**
		 * 
		 * @param {}
		 */
		p.bindView = function(view) {
			this.bindedView.push(view);
		};

		/**
		 * 
		 * @param {}
		 */
		p.add = function(r) {
			this.records.push(r);
		};

		p.removeAt = function(index) {
			this.records.splice(index, 1);
		};

		p.getAt = function(index) {
			return this.records[index];
		};

		/**
		 * 
		 * @param {}
		 */
		p.getRecords = function() {
			return this.records;
		};

		p.size = function() {
			return this.records.length;
		};



		/**
		 * 
		 * @param {}
		 */
		p.sort = function(sorts) {};

		/**
		 * 
		 * @param {}
		 */
		p.filter = function(filters) {};

		p.save = function() {

		};

		return BaseModel;
	})();

	module.exports = BaseModel;
	
})();