/**
 * 
 * @param
 * @return
 */
module.exports = ['$http','MovieRecord', function($http, MovieRecord){
	let store = function() {
		this.data = null;
		this.filtered = [];
		this.records = [];

		this.currentSearchKeyWord = null;

		/**
		 * cache the result to prevent too many same request
		 */
		this.cache = {};

		this.hasResults = false;

		/**
		 * init
		 * @param
		 * @return
		 */
		this.init();
	};

	let p = store.prototype;

	/**
	 * 
	 * @param
	 * @return
	 */
	p.init = function() {
		this.fetchFromLocalStorage();
	};

	/**
	 * 
	 * @param
	 * @return
	 */
	p.fetchMovies = function(val) {
		let me = this;

		// get value from cache first
		// if(me.cache[val]) {
		// 	return me.setData(me.cache[val]);
		// }

		// otherwise
		console.log(val);
		return $http({
			method: 'GET',
			url: 'https://jsonmock.hackerrank.com/api/movies/search/?Title=' + val,
		}).then(
			function(res) {
				// me.cache[val] = res.data.data;
				me.setData(res.data.data);
				me.refreshLocalStorage();
				me.currentSearchKeyWord = val;
			},
			function(err) {
				console.log(err);
			}
		);
	};

	/**
	 * 
	 * @param
	 * @return
	 */
	p.refreshLocalStorage = function() {
		let o = {};
		o.data = this.data;
		localStorage.setItem("fll-movies", JSON.stringify(o));
	};

	/**
	 * 
	 * @param
	 * @return
	 */
	p.fetchFromLocalStorage = function() {
		let data = localStorage.getItem("fll-movies");

		if(data && data !== 'undefined') {
			data = JSON.parse(data);
			this.setData(data.data);
		}
	};

	/**
	 * 
	 * @param
	 * @return
	 */
	p.setData = function(data) {
		this.data = data;
		this.onData();
	};

	/**
	 * 
	 * @param
	 * @return
	 */
	p.onData = function(newData) {
		this.refreshRecord(newData);
		this.hasResults = this.records.length > 0;
	};

	/**
	 * 
	 * @param
	 * @return
	 */
	p.filterBy = function(filter) {
		var t = [];
		for(let i = 0; i< this.records.length; i++) {
			if(t[filter.pro] === filter.val) {
				t.push(this.records[i]);
			}
		} 
		this.filtered = t;
	};

	/**
	 * 
	 * @param
	 * @return
	 */
	p.refreshRecord = function(data) {
		let rs = [];
		data = data || this.data;
		for(let i = 0; i < data.length; i++) {
			if(data[i].Type === 'movie'){
				rs.push(new MovieRecord({data: data[i]}));
			}
		}
		this.records = rs;
		return this.records;
	};
	return store;
}];
